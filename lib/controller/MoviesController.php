<?php

require_once(__DIR__.'/../model/db/MovieDb.php');

class MoviesController{
    
    public function listMovies(){
        $db = new MovieDb();
        return $db->listMovies();
    }
    
    public function detailMovie($idmovie){
        $db = new MovieDb();
        return $db->getMovie($idmovie);
    }
    
    public function deleteMovie($idmovie){
        $db = new MovieDb();
        return $db->deleteMovie($idmovie);
    }
    
    public function createMovie($title, $year, $director, $threed){
        $db = new MovieDb();
        return $db->insertMovie($title, $year, $director, $threed);
    }
    
    public function updateMovie($idmovie, $title, $year, $director, $threed){
        $db = new MovieDb();
        return $db->updateMovie($idmovie, $title, $year, $director, $threed);
    }
    
}