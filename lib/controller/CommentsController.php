<?php

require_once(__DIR__.'/../model/db/CommentDb.php');

class CommentsController{
    
    public function listComments($movie){
        $db = new CommentDb();
        return $db->listComment($movie);
    }
    
    public function createComment($user, $comment, $value, $movie){
        $db = new CommentDb();
        return $db->insertComment($user, $comment, $value, $movie);
    }
    
    public function hideComment($idc, $movid){
        $db = new CommentDb();
        return $db->hideComment($idc, $movid);
    }
    
}