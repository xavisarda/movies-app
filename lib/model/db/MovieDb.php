<?php

require_once(__DIR__.'/../Movie.php');

class MovieDb{
    
    private $conn = null;
    
    public function insertMovie($title, $year, $director, $threed){
        $this->openConnection();
        $query = "INSERT INTO movie (title, year, threed, director) VALUES (?, ?, ?, ?)";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("siis", $tn, $yn, $threen, $dirn);
        $tn = $title;
        $yn = $year;
        $threen = $threed;
        $dirn = $director;
        $stmt->execute();
        
        return $this->getMovie($this->conn->insert_id);
    }
    
    public function listMovies(){
        $this->openConnection();
        $query = "SELECT * FROM movie";
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        $res = $stmt->get_result();
        
        $result = array();
        while ($mv = $res->fetch_assoc() ) {
            array_push($result, new Movie($mv['id'], $mv['title'], $mv['year'], $mv['threed'], $mv['director']));
        }
        return $result;
    }
    
    public function getMovie($idmovie){
        $this->openConnection();
        $query = "SELECT * FROM movie WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idn);
        $idn = $idmovie; 
        $stmt->execute();
        $res = $stmt->get_result();

        $mv = $res->fetch_assoc();
        return new Movie($mv['id'], $mv['title'], $mv['year'], $mv['threed'], $mv['director']);
    }
    
    public function deleteMovie($idmovie){
        $this->openConnection();
        $query = "DELETE FROM movie WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idn);
        $idn = $idmovie; 
        $stmt->execute();
        
        return true;
    }
    
    public function updateMovie($idmovie, $title, $year, $director, $threed){
        $this->openConnection();
        $query = "UPDATE movie SET title = ?, year = ?, threed = ?, director = ? WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("siisi", $tn, $yn, $threen, $dirn, $idn);
        $tn = $title;
        $yn = $year;
        $threen = $threed;
        $dirn = $director;
        $idn = $idmovie; 
        $stmt->execute();
        
        return $this->getMovie($idmovie);
    }
    
    private function openConnection () {
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1", "xavisarda", null, "movies_app");    
        }
    }
    
}