<?php

require_once(__DIR__.'/../Comment.php');

class CommentDb{
    
    private $conn = null;
    
    public function insertComment($user, $comment, $value, $movie){
        $this->openConnection();
        $query = "INSERT INTO comment (title, comment, value, movie, visible) VALUES (?, ?, ?, ?, true)";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("ssii", $un, $cn, $vn, $mn);
        $un = $user;
        $cn = $comment;
        $vn = $value;
        $mn = $movie;
        $stmt->execute();
        
        return true;
    }
    
    public function listComment($movie){
        $this->openConnection();
        $query = "SELECT * FROM comment WHERE movie = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $mn);
        $mn = $movie;
        
        $stmt->execute();
        $res = $stmt->get_result();
        
        $result = array();
        while ($mv = $res->fetch_assoc() ) {
            array_push($result, new Comment($mv['id'], $mv['title'], $mv['comment'], $mv['value'], $mv['movie'], $mv['visible']));
        }
        return $result;
    }
    
    public function hideComment($idc, $movid){
        $this->openConnection();
        $query = "UPDATE comment SET visible = false WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $cn);
        $cn = $idc;
        
        $stmt->execute();
        $res = $stmt->get_result();
        
        return $movid;
    }
    
    private function openConnection () {
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1", "xavisarda", null, "movies_app");    
        }
    }
    
}