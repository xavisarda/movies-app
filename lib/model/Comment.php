<?php

class Comment{
    
    private $_id;
    private $_user;
    private $_comment;
    private $_value;
    private $_movie;
    private $_visible;
    
    public function __construct($id=null, $user=null, $comment=null, 
            $value=null, $movie=null, $visible=null){
        $this->_id = $id;
        $this->_user = $user; 
        $this->_comment = $comment;
        $this->_value = $value;
        $this->_movie = $movie;
        $this->_visible = $visible;
    }
    
    public function getId(){
        return $this->_id;
    }
    public function getUser(){
        return $this->_user;
    }
    public function getComment(){
        return $this->_comment;
    }
    public function getValue(){
        return $this->_value;
    }
    public function getMovie(){
        return $this->_movie;
    }
    public function isVisible(){
        return $this->_visible;
    }
    
    public function setId($var){
        $this->_id = $var;
    }
    public function setUser($var){
        $this->_user = $var;
    }
    public function setComment($var){
        $this->_comment = $var;
    }
    public function setValue($var){
        $this->_value = $var;
    }
    public function setMovie($var){
        $this->_movie = $var;
    }
    public function setVisible($var){
        $this->_visible = $var;
    }
    
}
