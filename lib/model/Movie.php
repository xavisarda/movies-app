<?php

class Movie{
    
    private $_id;
    private $_title;
    private $_year;
    private $_threed;
    private $_director;
    
    public function __construct($id=null, $title=null, $year=null, 
            $threed=null, $director=null){
        $this->_id = $id;
        $this->_title = $title; 
        $this->_year = $year;
        $this->_threed = $threed;
        $this->_director = $director;
    }
    
    public function getId(){
        return $this->_id;
    }
    public function getTitle(){
        return $this->_title;
    }
    public function getYear(){
        return $this->_year;
    }
    public function getThreed(){
        return $this->_threed;
    }
    public function getDirector(){
        return $this->_director;
    }
    
    public function setId($var){
        $this->_id = $var;
    }
    public function setTitle($var){
        $this->_title = $var;
    }
    public function setYear($var){
        $this->_year = $var;
    }
    public function setThreed($var){
        $this->_threed = $var;
    }
    public function setDirector($var){
        $this->_director = $var;
    }
    
}
