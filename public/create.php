<html>
    <head>
        <title>Movies Web App - New</title>
    </head>
    <body>
        <h1>New movie</h1>
        <form action="/forms/create.php" method="post">
            <label for="mtitle">Title</label><input type="text" name="mtitle" id="mtitle"/>
            <label for="myear">Year</label><input type="number" name="myear" id="myear"/>
            <label for="mdir">Director</label><input type="text" name="mdir" id="mdir"/>
            <label for="">3D:</label>
            <input type="radio" name="m3d" id="m3dY" value="1"/><label for="m3dY">Yes</label>
            <input type="radio" name="m3d" id="m3dN" value="0" checked/><label for="m3dN">No</label>
            <input type="submit" value="Create"/>
        </form>
        <a href="/">Back</a>
    </body>
</html>