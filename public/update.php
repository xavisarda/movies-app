<?php

require_once(__DIR__.'/../lib/controller/MoviesController.php');

$idmovie = $_GET['movie'];
$cnt = new MoviesController();
$movie = $cnt->detailMovie($idmovie);

?><html>
    <head>
        <title>Movies Web App - Detail</title>
    </head>
    <body>
        <h1><?=$movie->getTitle()?></h1>
        <form action="/forms/update.php" method="post">
            <label for="mtitle">Title</label><input type="text" name="mtitle" id="mtitle" value="<?=$movie->getTitle()?>"/>
            <label for="myear">Year</label><input type="number" name="myear" id="myear"  value="<?=$movie->getYear()?>"/>
            <label for="mdir">Director</label><input type="text" name="mdir" id="mdir" value="<?=$movie->getDirector()?>"/>
            <label for="">3D:</label>
            <input type="radio" name="m3d" id="m3dY" value="1" <?php if($movie->getThreed() == 1){ echo 'checked';} ?>/><label for="m3dY">Yes</label>
            <input type="radio" name="m3d" id="m3dN" value="0" <?php if($movie->getThreed() == 0){ echo 'checked';} ?>/><label for="m3dN">No</label>
            <input type="hidden" name="mid" value="<?=$movie->getId()?>"/>
            <input type="submit" value="Update"/>
        </form>
        <a href="/">Back</a>
        <a href="/delete.php?movie=<?=$movie->getId()?>">Delete</a>
    </body>
</html>