<?php

require_once(__DIR__.'/../lib/controller/MoviesController.php');

$cnt = new MoviesController();
$movies = $cnt->listMovies();

?><html>
    <head>
        <title>Movies Web App - List</title>
    </head>
    <body>
        <h1>Movies List</h1>
        <table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Year</th>
                    <th>Director</th>
                    <th>3D</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($movies as $movie){ ?>
                <tr>
                    <td><a href="/detail.php?movie=<?=$movie->getId()?>"><?=$movie->getTitle()?></a></td>
                    <td><?=$movie->getYear()?></td>
                    <td><?=$movie->getDirector()?></td>
                    <td><?=$movie->getThreed()?></td>
                    <td><a href="/update.php?movie=<?=$movie->getId()?>">Update</a></td>
                    <td><a href="/delete.php?movie=<?=$movie->getId()?>">Delete</a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <a href="/create.php">New movie</a>
    </body>
</html>