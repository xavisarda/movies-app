<?php

require_once(__DIR__.'/../lib/controller/MoviesController.php');

$idmovie = $_GET['movie'];
$cnt = new MoviesController();
$movie = $cnt->deleteMovie($idmovie);

?><html>
    <head>
        <title>Movies Web App - Delete</title>
    </head>
    <body>
        <h1>Movie successfully deleted</h1>
        <a href="/">Back</a>
    </body>
</html>