<?php

require_once(__DIR__.'/../lib/controller/MoviesController.php');
require_once(__DIR__.'/../lib/controller/CommentsController.php');

$idmovie = $_GET['movie'];
$cnt = new MoviesController();
$movie = $cnt->detailMovie($idmovie);

$cnt2 = new CommentsController();
$comments = $cnt2->listComments($idmovie);

?><html>
    <head>
        <title>Movies Web App - Detail</title>
    </head>
    <body>
        <h1><?=$movie->getTitle()?></h1>
        <ul>
            <li>Year: <?=$movie->getYear()?></li>
            <li>Director: <?=$movie->getDirector()?></li>
            <li>3D: <?=$movie->getThreed()?></li>
        </ul>
        <a href="/update.php?movie=<?=$movie->getId()?>">Update</a>
        <a href="/delete.php?movie=<?=$movie->getId()?>">Delete</a>
        <hr/>
        <h2>Comments</h2>
        <form action="/forms/comment.php" method="post">
            <label for="cuser">User</label><input type="text" name="cuser" id="cuser"/>
            <label for="ccomment">Comment</label><input type="text" name="ccomment" id="ccomment"/>
            <label for="cvalue">Value</label><input type="number" name="cvalue" id="cvalue"/>
            <input type="hidden" name="mid" value="<?=$movie->getId()?>"/>
            <input type="submit" value="Comment"/>
        </form>
        <ul>
            <?php foreach($comments as $comment){ 
                if($comment->isVisible()){ ?>
            <li>User: <?=$comment->getUser()?> - <?=$comment->getComment()?> (value:<?=$comment->getValue()?>) 
                <a href="/forms/hide.php?idc=<?=$comment->getId()?>&movid=<?=$movie->getId()?>">report</a>
            </li>
            <?php }else{ ?>
            <li>Comment reported as spam</li>
            <?php }} ?>
        </ul>
    </body>
</html>