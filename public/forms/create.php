<?php

require_once(__DIR__.'/../../lib/controller/MoviesController.php');

$cnt = new MoviesController();
$movie = $cnt->createMovie($_POST['mtitle'], $_POST['myear'], 
        $_POST['mdir'], $_POST['m3d']);

?><html>
    <head>
        <title>Movies Web App - Create</title>
    </head>
    <body>
        <h1><?=$movie->getTitle()?></h1>
        <p>Movie created</p>
        <ul>
            <li>Year: <?=$movie->getYear()?></li>
            <li>Director: <?=$movie->getDirector()?></li>
            <li>3D: <?=$movie->getThreed()?></li>
        </ul>
         <a href="/">Home</a>
        <a href="/update.php?movie=<?=$movie->getId()?>">Update</a>
        <a href="/delete.php?movie=<?=$movie->getId()?>">Delete</a>
    </body>
</html>